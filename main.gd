extends Node3D


var presents = []
var score = 0
var SANTA_SPEEDUP = 50

# Called when the node enters the scene tree for the first time.
func _ready():
	restart()
	pass # Replace with function body.

var bump_next = 6
var bump = 2
var accum: float = 0
func _process(delta):
	if Input.is_action_just_pressed('restart') :
		restart()
		return 
	
	if $'rb-santa'.position.y > 1100 or $'rb-santa'.position.y < -100 :
		$l_game_over.visible = true
	
	accum = accum + delta 
	
	if $'rb-santa'.linear_velocity.length() < $"rb-santa".base_speed:
		$"rb-santa".linear_velocity = $"rb-santa".linear_velocity.normalized() * $"rb-santa".base_speed
	
	for present in presents:
		var p = present as RigidBody2D
		if p.linear_velocity.length() > 1000:
			p.linear_velocity = p.linear_velocity.limit_length(1000)
				
		if present.position.y > 1100:
			present.queue_free()
			print('Freed ', present)
			presents.erase(present)
			break

	
	if accum > bump :
		var pres = preload("res://present.tscn").instantiate()
		#pres.set_meta('tag', 'present') 
		#pres.has_meta('tag')
		pres.position.x = 300
		pres.position.y = 100
		var p = pres as RigidBody2D
		p.linear_velocity.x = 10
		p.linear_velocity.y = 100
		presents.push_back(pres)
		add_child(pres)
		bump += bump_next

	pass


func restart():
	accum = 0
	bump = 2
	presents.filter(func(p): 
		p.queue_free() 
		return false 
	)
	presents.clear()
	$"rb-santa".global_transform.origin = Vector2(500, 200)
	$"rb-santa".linear_velocity = Vector2(100, 600)
	$"rb-santa".base_speed = 100
	$"rb-santa".linear_velocity = $"rb-santa".linear_velocity.normalized() * $"rb-santa".base_speed
	score = 0
	$l_score.text = str(score)
	$l_game_over.visible = false


func _on_rbsanta_body_exited(body):
	print('Collision santa with', body)
	if body.has_meta('tag') and body.get_meta('tag') == 'present':
		body.queue_free()
		presents.erase(body)
		$"rb-santa".base_speed += SANTA_SPEEDUP
		score += 1
		$l_score.text = str(score)
	pass # Replace with function body.
