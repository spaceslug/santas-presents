extends MeshInstance3D

@export var trans_x: float = 0.0
@export var trans_y: float = 0.0
@export var scale_trans: float = 1.0



# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


var accum: float = 0
func _process(delta):
	accum = accum + delta 
	mesh.material.set_shader_parameter("time", accum)
	mesh.material.set_shader_parameter("trans_x", trans_x)
	mesh.material.set_shader_parameter("trans_y", trans_y)
	mesh.material.set_shader_parameter("scale_trans", scale_trans)
